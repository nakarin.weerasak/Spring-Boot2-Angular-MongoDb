package com.example.springapi.controller;


import com.example.springapi.model.Content;
import com.example.springapi.repository.ContentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/content")
public class ContentController {
	
	@Autowired
	private ContentRepository contentRepository;

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public void addContent(@RequestBody Content content) {

		contentRepository.save(content);

	}
	

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public void updateContent(@RequestBody Content content) {
		System.out.println(content.getName());
		contentRepository.save(content);
	}
	

	@RequestMapping(value = "/contents", method = RequestMethod.GET)
	public List<Content> getContents() {
		List<Content> lt = contentRepository.findAll();
		
		for(int i = 0; i < lt.size();i++)
		{
			System.out.println(lt.get(i));
		}
		
		for(Content contents : contentRepository.findAll()) {
			System.out.println(contents.getName());
		}
		
		return lt;
	}


	@RequestMapping(value = "/content/{id}", method = RequestMethod.GET)
	public Content getContent(@PathVariable Integer id){
		Optional<Content> optUser = contentRepository.findById(id); // returns java8 optional
		if (optUser.isPresent()) {
			return optUser.get();
		} else {
			return null;
		}
	}
}
