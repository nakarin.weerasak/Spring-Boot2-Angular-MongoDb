import { TestBed, inject } from '@angular/core/testing';

import { AllfunctionService } from './allfunction.service';

describe('AllfunctionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AllfunctionService]
    });
  });

  it('should be created', inject([AllfunctionService], (service: AllfunctionService) => {
    expect(service).toBeTruthy();
  }));
});
