package com.example.springapi.repository;


import com.example.springapi.model.Content;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface ContentRepository extends MongoRepository<Content, Integer> {


}
