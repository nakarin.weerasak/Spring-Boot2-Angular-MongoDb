import { Component, OnInit } from '@angular/core';
import {AllfunctionService} from '../allfunction.service'

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: 'user', title: 'User Profile',  icon:'pe-7s-user', class: '' },
    { path: 'content', title: 'Contents',  icon: 'pe-7s-graph', class: '' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor(private allfunction:AllfunctionService) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }

  Logout(){
    this.allfunction.Logout();
    }

  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };
}
