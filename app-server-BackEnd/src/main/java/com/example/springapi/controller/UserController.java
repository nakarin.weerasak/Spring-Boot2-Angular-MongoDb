package com.example.springapi.controller;


import com.example.springapi.exception.ResourceNotFoundException;
import com.example.springapi.model.User;
import com.example.springapi.payload.*;
import com.example.springapi.repository.RoleRepository;
import com.example.springapi.repository.UserRepository;
import com.example.springapi.security.UserPrincipal;
import com.example.springapi.security.CurrentUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;


@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;


    @Autowired
    PasswordEncoder passwordEncoder;

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @GetMapping("/user/me")
    @PreAuthorize("hasRole('USER')")
    public UserSummary getCurrentUser(@CurrentUser UserPrincipal currentUser) {
        UserSummary userSummary = new UserSummary(currentUser.getId(), currentUser.getUsername(), currentUser.getName());
        return userSummary;
    }

    @GetMapping("/user/checkUsernameAvailability")
    public UserIdentityAvailability checkUsernameAvailability(@RequestParam(value = "username") String username) {
        Boolean isAvailable = !userRepository.existsByUsername(username);
        return new UserIdentityAvailability(isAvailable);
    }

    @GetMapping("/user/checkEmailAvailability")
    public UserIdentityAvailability checkEmailAvailability(@RequestParam(value = "email") String email) {
        Boolean isAvailable = !userRepository.existsByEmail(email);
        return new UserIdentityAvailability(isAvailable);
    }

    @GetMapping("/users/{username}")
    public UserProfile getUserProfile(@PathVariable(value = "username") String username) {


        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new ResourceNotFoundException("User", "username", username));

        UserProfile userProfile = new UserProfile(user.getId(), user.getUsername(), user.getName(), user.getEmail(), user.getCreatedAt());

            return userProfile;

    }

    @PostMapping("/users/update/{userId}")
    public User updateProfile(@PathVariable Long userId, @Valid @RequestBody User user) {
        return  userRepository.findById(userId).map(post -> {
            post.setName(user.getName());
            post.setUsername(user.getUsername());
            post.setPassword(passwordEncoder.encode(user.getPassword()));
            return userRepository.save(post);
        }).orElseThrow(() -> new ResourceNotFoundException("User", "username", user.getName()));
    }



}
