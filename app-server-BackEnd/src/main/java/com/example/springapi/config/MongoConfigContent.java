package com.example.springapi.config;

import com.example.springapi.model.Content;
import com.example.springapi.repository.ContentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableMongoRepositories(basePackageClasses = ContentRepository.class)
@Configuration
public class MongoConfigContent implements CommandLineRunner {

    @Autowired
    ContentRepository contentRepository;


    @Override
    public void run(String... args) throws Exception {
        contentRepository.save(new Content(1, "JAVA Sping Content", "How to complete this guide Like most Spring Getting Started guides, you can start from scratch and complete each step, or you can bypass basic setup steps that are already familiar to you. Either way, you end up with working code."));
        contentRepository.save(new Content(2, "Angular Spring Content", "Angular is a platform that makes it easy to build applications with the web. Angular combines declarative templates, dependency injection, end to end tooling, and integrated best practices to solve development challenges."));
    }
}
