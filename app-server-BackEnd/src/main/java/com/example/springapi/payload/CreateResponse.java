package com.example.springapi.payload;

/**
 * Created by rajeevkumarsingh on 19/08/17.
 */
public class CreateResponse {
    private String username;
    private String status;
    private String accessToken="eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwiaWF0IjoxNTI3NTg3OTYyLCJleHAiOjE1MjgxOTI3NjJ9.rZgxJPj7h48oqc4AiyHDzIfR33T7l-toNaYKPS0UnXc5l9xGzFXY5hS2olEwpZAnlkzcgrHlCn2Ej07s6KJBtQ";

    public CreateResponse(String status, String user) {
        this.status = status;
        this.username = user;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

}
