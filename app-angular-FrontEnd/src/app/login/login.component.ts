import { Component, OnInit } from '@angular/core';
import { login, signup, getCurrentUser } from '../util/apiutils.service';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { ACCESS_TOKEN,ACCESS_USERNAME } from '../const/val.service';
import { AppComponent } from "../app.component";
export const API_BASE_URL = 'http://localhost:5000/api';
import { map } from 'rxjs/operators';
import { Router } from "@angular/router";
import { AllfunctionService } from '../allfunction.service'


const swal = require('sweetalert2')


const request = (options) => {
    const headers = new Headers({
        'Content-Type': 'application/json',
    })


    if (localStorage.getItem(ACCESS_TOKEN)) {
        headers.append('Authorization', 'Bearer ' + localStorage.getItem(ACCESS_TOKEN))
    }

    const defaults = { headers: headers };
    options = Object.assign({}, defaults, options);

    return fetch(options.url, options)
        .then(response =>
            response.json().then(json => {
                if (!response.ok) {
                    return Promise.reject(json);
                }
                return json;
            })
        );
};


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {



    username: String;
    password: String;
    

    message: String;
    constructor(public http: Http, private router: Router, private allfunce:AllfunctionService) { }

    ngOnInit() {


    }



    Login(us, ps) {
        const loginRequest = Object.assign({}, { usernameOrEmail: us, password: ps });

        console.log(loginRequest)
        const val = {
            url: API_BASE_URL + "/auth/signin",
            method: 'POST',
            body: JSON.stringify(loginRequest)
        };
        login(loginRequest)
            .then(response => {

                console.log(response["accessToken"]);
                localStorage.setItem(ACCESS_TOKEN, response["accessToken"]);
                localStorage.setItem(ACCESS_USERNAME, response["username"]);

                if (localStorage.getItem(ACCESS_TOKEN) != undefined) {
                    swal({
                        position: 'top-end',
                        type: 'success',
                        title: 'ยินดีต้อนรับ',
                        showConfirmButton: false,
                        timer: 1500
                    })
                    this.router.navigate(['/user']);
                }
                else {

                    this.router.navigate(['/user']);
                    this.router.navigate(['/login']);
                    localStorage.removeItem(ACCESS_TOKEN);
                }

            }).catch(error => {

                swal({
                    position: 'top-end',
                    type: 'error',
                    title: 'ผิดพลาด ',
                    showConfirmButton: false,
                    timer: 1500
                })

                this.username = null;
                this.password = null;



                if (error.status === 401) {

                } else {

                }
            });



    };


    Signup(na, em, us, ps) {
        const signupRequest = {
            name: na,
            email: em,
            username: us,
            password: ps
        };

        signup(signupRequest)
            .then(response => {
                swal({
                    position: 'top-end',
                    type: 'success',
                    title: response["message"],
                    showConfirmButton: false,
                    timer: 1500
                })
                if (response["success"] == true) {

                    setTimeout(() => {
                        window.location.reload();

                    },3000);

                }

            }).catch(error => {
                console.log(error);
                swal({
                    position: 'top-end',
                    type: 'error',
                    title: "เกิดข้อผิดพลาด "+error["message"],
                    showConfirmButton: false,
                    timer: 1500
                })
            });
    }





}
