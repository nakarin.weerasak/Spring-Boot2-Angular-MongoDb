import {map} from 'rxjs/operators';
import { Http, Headers, RequestOptions,Response} from '@angular/http';
export const API_BASE_URL = 'http://localhost:5000/api';
export const API_BASE_URL_CONTENT = 'http://localhost:5000';
//export const API_BASE_URL = '/api';
import { ACCESS_TOKEN,ACCESS_USERNAME } from '../const/val.service';

export const POLL_LIST_SIZE = 30;
export const MAX_CHOICES = 6;
export const POLL_QUESTION_MAX_LENGTH = 140;
export const POLL_CHOICE_MAX_LENGTH = 40;

export const NAME_MIN_LENGTH = 4;
export const NAME_MAX_LENGTH = 40;

export const USERNAME_MIN_LENGTH = 3;
export const USERNAME_MAX_LENGTH = 15;

export const EMAIL_MAX_LENGTH = 40;

export const PASSWORD_MIN_LENGTH = 6;
export const PASSWORD_MAX_LENGTH = 20;

import { Injectable } from '@angular/core';




const request = (options) => {
    const headers = new Headers({
        'Content-Type': 'application/json',
    })
    
    
    if(localStorage.getItem(ACCESS_TOKEN)) {
        headers.append('Authorization', 'Bearer ' + localStorage.getItem(ACCESS_TOKEN))
    }

    const defaults = {headers: headers};
    options = Object.assign({}, defaults, options);
    
    return fetch(options.url, options)
    .then(response => 
        response.json().then(json => {
            if(!response.ok) {
                return Promise.reject(json);
            }
            return json;
        })
    );
};

export function getAllPolls(page, size) {
  page = page || 0;
  size = size || POLL_LIST_SIZE;

  return request({
      url: API_BASE_URL + "/polls?page=" + page + "&size=" + size,
      method: 'GET'
  });
}

export function updateProfile(profileupdate,id) {
  return request({
      url: API_BASE_URL + "/users/update/"+id,
      method: 'POST',
      body: JSON.stringify(profileupdate),
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem(ACCESS_TOKEN),
        'Content-Type': 'application/json'
    }
  });
}

export function castVote(voteData) {
  return request({
      url: API_BASE_URL + "/polls/" + voteData.pollId + "/votes",
      method: 'POST',
      body: JSON.stringify(voteData)
  });
}

export function login(loginRequest) {
  return request({
      url: API_BASE_URL + "/auth/signin",
      method: 'POST',
      body: JSON.stringify(loginRequest),
      headers: {
        'Content-Type': 'application/json'
    }
  });
}


export function signup(signupRequest) {
    console.log(signupRequest);
  return request({
      url: API_BASE_URL + "/auth/signup",
      method: 'POST',
      body: JSON.stringify(signupRequest),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem(ACCESS_TOKEN)
    }
  });
}

export function checkUsernameAvailability(username) {
  return request({
      url: API_BASE_URL + "/user/checkUsernameAvailability?username=" + username,
      method: 'GET'
  });
}

export function checkEmailAvailability(email) {
  return request({
      url: API_BASE_URL + "/user/checkEmailAvailability?email=" + email,
      method: 'GET'
  });
}


export function getCurrentUser() {
    if(!localStorage.getItem(ACCESS_TOKEN)) {
        return Promise.reject("No access token set.");
    }

    const username= localStorage.getItem(ACCESS_USERNAME)

    return request({
        url: API_BASE_URL + "/users/"+username,
        method: 'GET' 
    });
}

export function getUserProfile(username) {
  return request({
      url: API_BASE_URL + "/users/" + username,
      method: 'GET'
  });
}

export function getContent(id) {
    return request({
        url: API_BASE_URL_CONTENT + "/content/content/"+id,
        method: 'GET'
    });
  }

  export function updateContent(modelContent) {
    return request({
        url: API_BASE_URL_CONTENT + "/content/update",
        method: 'POST',
        body: JSON.stringify(modelContent),
        headers: {
          'Content-Type': 'application/json'
      }
    });
  }