import { Component, OnInit } from '@angular/core';
import { LocationStrategy, PlatformLocation, Location } from '@angular/common';
import { Router } from "@angular/router";
import { ACCESS_TOKEN } from './const/val.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    static API_URL="http://localhost:5000"
     constructor(public location: Location,private router : Router) {}

    ngOnInit(){


    }

    clickLoginstatus:Boolean=false;

    checklogin(){
      if(localStorage.getItem(ACCESS_TOKEN)==null){
        // this.router.navigate(['/login']);
        return false;
      }
      else{
        return true;
      }
    }

    clicklogin(val){
      this.clickLoginstatus=val;
      return val;
    }

    isMap(path){
      var titlee = this.location.prepareExternalUrl(this.location.path());
      titlee = titlee.slice( 1 );
      if(path == titlee){
        return false;
      }
      else {
        return true;
      }
    }
}
